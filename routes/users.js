var express = require('express');
var router = express.Router();
var db = require("../mySqlDB");
var redisDb = require('../redisDB');
var request = require('request');
var _ = require('underscore');

const crypto = require('crypto');
const algorithm = 'aes-256-ctr';
const private_key = "zBS27wqnSvWfBqUe9T5sCceQWkmnduypUgZR";

// router.post('/', function (req, res, next) {    
//     console.log("env details are ",global.bs);
// })
/* post users listing. */


router.post('/verifyDetails', function (req, res, next) {
    getModels()

    if (!req.headers.client_id ||
        !req.headers.client_secret ||
        !req.headers.response_type ||
        !req.headers.private_key ||
        !req.headers.ts) {
        return res.status(400).send({ status: 0, message: "Required parameters are missing." });
    }
    var client_id = req.headers.client_id;
    var client_secret = req.headers.client_secret;
    var response_type = req.headers.response_type;
    var timestamp = req.headers.ts;

    if (((new Date().getTime()) - timestamp * 1000 <= 30000))//30 seconds
    {
        if(req.headers.private_key!=private_key){
            return res.status(400).send({ status: 0, message: "Invalid private key." });
        }
        
        var query = { client_id: client_id, client_secret_key: client_secret, active_status: true };
        console.log("query time is " + JSON.stringify(query));
        this.openapiModel.findAll({ where: query }).then(function (companyDetailsResult) {
            if (companyDetailsResult.length > 0) {
                var redisKey = "sdktoken_" + client_id
                //var password =Math.round((Math.pow(36, 16 + 1) - Math.random() * Math.pow(36, 16))).toString(36).slice(1);
                var password = crypto.randomBytes(16);
                var key = crypto.randomBytes(32);
                // Math.random().toString(36).slice(2)
                var dataEncyptIs=encrypt(password + "_" + req.headers.ts + "_" + client_id + "_" + req.headers.private_key,key,password);
                var obj = {
                    company_id: companyDetailsResult[0]._id,
                    type: dataEncyptIs.type,
                    key:dataEncyptIs.key,
                    code:dataEncyptIs.data
                }
                redisDb.setDataWithTTL(redisKey, JSON.stringify(obj), 50)
                return res.status(200).send({ status: 1, message: "Resource available.", data: obj });
            }
            else {
                return res.status(401).send({ status: 0, message: "Invalid client_id and client_secret." });
            }
        }).catch(function (err) {
            console.log("error " + JSON.stringify(err.message));
            return res.status(500).send({ status: 0, message: "Something went wrong please try again." });
        })

    }
    else {
        return res.status(400).send({ status: 0, message: "Session has expired. Please try again." });
    }
});
router.post('/getDetails', function (req, res, next) {
    getModels()
    if (!req.headers.client_id ||
        !req.headers.private_key ||
        !req.headers.response_type ||
        !req.headers.mobile_key ||
        !req.headers.code ||
        !req.headers.ts) {
        return res.status(400).send({ status: 0, message: "Required parameters are missing." });
    }
    var client_id = req.headers.client_id;
    var response_type = req.headers.response_type;
    var timestamp = req.headers.ts;

    if (((new Date().getTime()) - timestamp * 1000 <= 30000))//30 seconds
    {
        if(req.headers.private_key!=private_key){
            return res.status(400).send({ status: 0, message: "Invalid private key." });
        }
        var query = { client_id: client_id, active_status: true };
        this.openapiModel.findAll({ where: query }).then(function (companyDetailsResult) {
            if (companyDetailsResult.length > 0) {
                var redisKey = "sdktoken_" + client_id
                redisDb.getEmailData(redisKey, function (err, obj) {
                    if (err || !obj) {
                        return res.status(400).send({ status: 0, message: "Session has expired. Please try again." });
                    }
                    else {
                        //.code:encrypt(password+"_"+req.headers.ts+"_"+client_id+"_"+req.headers.private_key)

                        var decodeObject = decrypt(obj.code,obj.key ,obj.type);
                        var code = decodeObject.split("_")
                        var time = parseFloat(code[1])
                        var codeClientId = code[2]
                        var codePrivateKey = code[3]

                        console.log(`code ${obj.code} time ${time} codeClientId ${codeClientId} codePrivateKey ${codePrivateKey}`);
                        if (req.headers.code != obj.code || client_id != codeClientId || req.headers.private_key != codePrivateKey) {
                            return res.status(401).send({ status: 0, message: "Invalid client details." });
                        }
                        else {
                            var currentTime = new Date().getTime() / 1000;
                            console.log("time is " + time);
                            console.log("currentTime is " + currentTime);
                            var timeDiff = parseInt(currentTime) - parseInt(time);
                            console.log("timeDiff is " + timeDiff);

                            if (timeDiff > 30) {
                                console.log("time diff is");
                                return res.status(400).send({ status: 0, message: "Session has expired. Please try again." });
                            }
                            else {
                                //var password = Math.round((Math.pow(36, 16 + 1) - Math.random() * Math.pow(36, 16))).toString(36).slice(1);
                                //Math.random().toString(36).slice(2)

                                var urls = {
                                    register: "user/register",
                                    add_hub: "hub/addDemoHUbId",
                                    get_hub: "hub/getHubs",
                                    get_rooms: "room/getRoomList",
                                    device: "hub/getDevicesCount",
                                    adddevice: "device/addDevice",
                                    setStatus: "event/SetStatus",
                                    addHubLatest:"hub/addNewHublatest",
                                    mqttURL:"https://smartmqtt.b1automation.com",
                                    deleteDevice:"device/deleteDevice",
                                    connectSocket:"event/connectSmartSocket",
                                    accountLinking:"smartthings/accountlinking",
                                    timestamp: timestamp
                                }

                                var password = crypto.randomBytes(16);
                                var key = crypto.randomBytes(32);
                                var encyptObjis=encrypt(JSON.stringify(urls),key,password);
                                var obj = {
                                    details:encyptObjis.data,
                                    type: encyptObjis.type,
                                    key:encyptObjis.key
                                }
                                console.log("send sensitive data is " + obj);
                                return res.status(200).send({ status: 1, message: "Resource available.", data: obj });
                            }
                        }

                    }
                })
            }
            else {
                return res.status(401).send({ status: 0, message: "Invalid client_id and client_secret." });
            }
        }).catch(function (err) {
            console.log("error " + JSON.stringify(err.message));
            return res.status(500).send({ status: 0, message: "Something went wrong please try again." });
        })
    }
    else {
        return res.status(400).send({ status: 0, message: "Session has expired. Please try again." });
    }
});
router.post('/userexist', function (req, res, next) {
    getModels()
    console.log("get header details ",req.headers);
    console.log("get body details ",req.body);
    if (!req.headers.client_id ||
        !req.headers.response_type ||
        !req.headers.ts ||
        !req.body.userID||
        !req.body.app_device_token_id||
        !req.body.device_type ||
        !req.body.origin_id ) {
        return res.status(400).send({ status: 0, message: "Required parameters are missing." });
    }
    var client_id = req.headers.client_id;
    var response_type = req.headers.response_type;
    var timestamp = req.headers.ts;

    if (((new Date().getTime()) - timestamp * 1000 <= 30000))//30 seconds
    {
        var query = { client_id: client_id, active_status: true };
        this.openapiModel.findAll({ where: query }).then(function (companyDetailsResult) {
            if (companyDetailsResult.length > 0) {
                this.smartThingsModel.findAll({ where: { install_app_id: req.body.userID } }).then(function (smartThingsUser) {
                    if (smartThingsUser.length > 0) {
                        var userdetails= JSON.parse(JSON.stringify(smartThingsUser[0]))
                       // var password = Math.round((Math.pow(36, 16 + 1) - Math.random() * Math.pow(36, 16))).toString(36).slice(1);
                       //var password = crypto.randomBytes(16); 
                       //Math.random().toString(36).slice(2)
                        var sessionId = generateSessionId();
                        userdetails.session_id=sessionId;
                        var password = crypto.randomBytes(16);
                        var key = crypto.randomBytes(32);
                        var data=encrypt(JSON.stringify(userdetails),key,password);
                        var obj = {
                            app_device_token_id: req.body.app_device_token_id,
                            user_id: userdetails.user_id,
                            act_status: "1",
                            sessionId: sessionId,
                            device_type: req.body.device_type || 'IOS',
                            org_id:req.body.origin_id||'0'
                        };
                        this.userDevModel.destroy({where:{app_device_token_id: req.body.app_device_token_id}}).then(function(){
                            var userDev = this.userDevModel.build(obj);
                            userDev.save()
                        .then(function () {
                            return res.status(200).send({ status: 1, message: "User exist.",data:{type:data.type,key:data.key,details:data.data} }); 
                        })
                        .catch(function (err) {
                            console.log(err.message);
                            return res.status(500).send({ status: 0, message: "Something went wrong please try again." });
                        });
                        }).catch(function(err){
                            return res.status(500).send({ status: 0, message: "Something went wrong please try again." });
                        })
                        

                        
                    }
                    else {
                        return res.status(200).send({ status: 0, message: "User dosen't exist." });
                    }
                })
            }
        }).catch(function (err) {
            return res.status(500).send({ status: 0, message: "Something went wrong please try again." });
        })

    }
    else {
        return res.status(400).send({ status: 0, message: "Session has expired. Please try again." });
    }
});
router.post('/send', function (req, res, next) {
    getModels()
    if (!req.headers.client_id ||
        !req.headers.ts ||
        !req.body.type ||
        !req.body.data ||
        !req.body.key) {
        return res.status(400).send({ status: 0, message: "Required parameters are missing." });
    }
    var timestamp = req.headers.ts;
    if (((new Date().getTime()) - timestamp * 1000 <= 30000))//30 seconds
    {
        clientIdvalidation(req, res, function (err, data) {
            if (err) {
                return res.status(500).send({ status: 0, message: "Something went wrong. Please try again later."+err });
            }
            else {
                var body = req.body;
                var decodeObj = decrypt(body.data,body.key,body.type);
                var reqObject=JSON.parse(decodeObj);
                var endPointurl =reqObject.url;
                var method =reqObject.method || "POST";
                if(reqObject.org_id){
                    reqObject.origin_id=reqObject.org_id
                }
                delete reqObject.url;
                delete reqObject.method;
                console.log("request object is ",reqObject);
                var options = {
                'method': method,
                'url': 'https://smart.b1automation.com/'+endPointurl,
                'headers': {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(reqObject)

                };
                console.log("log details are "+JSON.stringify(options));

                request(options, function (error, response,body) { 
                if (error) {
                    return res.status(500).send({ status: 0, message: "Something went wrong please try again." });   
                }
                else{
                    console.log("log details are "+JSON.stringify(body));
                    var password = crypto.randomBytes(16);
                    var key = crypto.randomBytes(32);
                    var responseBodyIs=Object.assign({},JSON.parse(body)) ;
                    var respoObjForm;
                    if(endPointurl=="hub/addNewHublatest"){
                        respoObjForm={
                            status:responseBodyIs.status,
                            message:responseBodyIs.message,
                        }
                    }else{
                        respoObjForm= responseBodyIs;
                    }
                    var data=encrypt(JSON.stringify(respoObjForm),key,password);
                    var obj= {type:data.type,key:data.key,details:data.data}
                    
                    return res.status(200).send(obj);

                }
                });
            }
        })
    }
    else {
        return res.status(400).send({ status: 0, message: "Session has expired. Please try again." });
    }

});
function clientIdvalidation(req, res, callback) {
    var query = { client_id: req.headers.client_id, active_status: true };
    console.log("query time is " + JSON.stringify(query));
    this.openapiModel.findAll({ where: query }).then(function (companyDetailsResult) {
        if (companyDetailsResult.length > 0) {
            callback(null, companyDetailsResult);
        }
        else {
            return res.status(401).send({ status: 0, message: "Invalid client_id." });
        }
    }).catch(function (err) {
        console.log("error ",err)
        callback(err, null);
    })
}
// router.post('/socket/v2/token', function (req, res, next) {
//     getModels()
//     if (!req.headers.client_id ||
//         !req.headers.grant_type ||
//         !req.headers.ts ||
//         !req.body.hub_id) {
//         return res.status(400).send({ status: 0, message: "Required parameters are missing." });
//     }
//     else {
//         var timestamp = req.headers.ts;
//         var client_id = req.headers.client_id;
//         var grant_type = req.headers.grant_type;
//         var hub_id = req.body.hub_id;

//         console.log("current time is " + (new Date().getTime()));
//         console.log("given time is " + timestamp * 1000);

//         if (((new Date().getTime()) - timestamp * 1000 <= 30000))//30 seconds
//         {
//             if (grant_type == "code")//This is when we get multiple hub concept
//             {
//                 this.userHubModel.belongsTo(this.hubDefModel, { foreignKey: 'hub_id', targetKey: 'hub_id' });
//                 this.hubDefModel.belongsTo(this.hubModel, { foreignKey: 'hub_id', targetKey: 'hub_id' });
//                 this.userHubModel.findAll({
//                     where: {
//                         hub_id: hub_id,
//                         act_enabled: true
//                     }, include: [
//                         {
//                             model: this.hubDefModel,
//                             include: [{
//                                 model: this.hubModel
//                             },
//                             ],
//                         }
//                     ],
//                     hub_id: {
//                         $and: {
//                             $ne: null,
//                             $ne: ""
//                         }
//                     }
//                 }).then(function (hubs) {
//                     if (hubs.length > 0) {

//                         var loginStsKey = "openapi_login_state_" + req.headers.client_id;
//                         redisDb.getEmailData(loginStsKey, function (err, redisObj) {
//                             if (err || !redisObj) {
//                                 return res.status(400).send({ status: 0, message: "Session has expired. Please try again with new code." });
//                             }
//                             else {
//                                 var hubData = [];
//                                 _.each(hubs, function (hub) {
//                                     var hubObj = {};
//                                     hubObj.hub_id = hub.dataValues.hub_id;
//                                     hubData.push(hubObj);
//                                 });

//                                 var obj = {
//                                     hubs: hubData,
//                                     user: redisObj.user,
//                                     company_id: redisObj.company_id,
//                                     code: generateSessionId(),
//                                     expires_in_sec: 3600,
//                                     ts: new Date().getTime()
//                                 }
//                                 redisDb.setDataWithTTL("openapi_login_state_" + hub_id, JSON.stringify(obj), 3600);
//                                 delete obj.user;
//                                 delete obj.company_id;
//                                 return res.status(200).send({ status: 1, message: "Successfully retrived the data.", data: obj });
//                             }
//                         })



//                     }
//                     else {
//                         return res.status(200).send({ status: 0, message: "No hubs are associated with your account." });
//                     }
//                 });
//             }
//             else if (grant_type == "token")// This is when client wants to have new access token
//             {
//                 if (!req.body.code) {
//                     return res.status(400).send({ status: 0, message: "Required parameters are missing." });
//                 }
//                 var loginStsKey = "openapi_login_state_" + hub_id;
//                 redisDb.getEmailData(loginStsKey, function (err, redisObj) {
//                     if (err || !redisObj) {
//                         return res.status(400).send({ status: 0, message: "Code session has expired. Please try again with new code." });
//                     }
//                     else {
//                         console.log("redisObj is " + JSON.stringify(redisObj));
//                         if (redisObj.code == req.body.code) {
//                             this.openapiModel.findAll({ where: { client_id: client_id } }).then(function (companyDetails) {
//                                 console.log("Company details are " + JSON.stringify(companyDetails));

//                                 if (companyDetails.length > 0) {
//                                     this.opentokenModel.findAll({ attributes: ['_id', 'user_id', 'company_id', 'email_id', 'hub_id', 'accesstoken', 'refreshtoken', 'expiry_date'], where: { hub_id: hub_id, company_id: companyDetails[0]._id } }).then(function (hubTokenDetails) {
//                                         if (hubTokenDetails.length > 0) {
//                                             var objHubs = Object.assign({}, hubTokenDetails[0].dataValues);

//                                             var expiryDate = new Date();
//                                             expiryDate.setMonth(expiryDate.getMonth() + 3);
//                                             var updateObj = { expiry_date: expiryDate }
//                                             if (redisObj.user.user_id != objHubs.user_id) {
//                                                 updateObj.user_id = redisObj.user.user_id;
//                                                 updateObj.email_id = redisObj.user.email_id;
//                                             }

//                                             this.opentokenModel.update(updateObj, { where: { _id: objHubs._id } }).then(function () {
//                                                 //sendrequest to web socket for registeration
//                                                 var obj = Object.assign({}, objHubs);
//                                                 obj.user_id = redisObj.user.user_id;
//                                                 obj.email_id = redisObj.user.email_id;
//                                                 obj.expiry_date = expiryDate;
//                                                 delete obj._id;

//                                                 sendingRequestToSubHub(obj, "allsubscribe");
//                                                 return res.status(200).send({ status: 1, message: "Successfully retrieved.", data: obj });

//                                             }).catch(function (error) {
//                                                 console.log("error:" + error.message);
//                                                 return res.status(500).send({ status: 0, message: "Something went wrong. Please try again later." });
//                                             })
//                                         }
//                                         else {
//                                             var access_token = generateSessionId();
//                                             var refresh_token = redisObj.user.user_id + "_" + generateSessionId();
//                                             var expiryDate = new Date();
//                                             var tokenData = {};
//                                             expiryDate.setMonth(expiryDate.getMonth() + 3);
//                                             tokenData.user_id = redisObj.user.user_id;
//                                             tokenData.company_id = companyDetails[0]._id;
//                                             tokenData.user_name = redisObj.user.first_name;
//                                             tokenData.email_id = redisObj.user.email_id;
//                                             tokenData.hub_id = hub_id;
//                                             tokenData.accesstoken = access_token;
//                                             tokenData.refreshtoken = refresh_token;
//                                             tokenData.expiry_date = expiryDate;

//                                             console.log("mysql saving data" + JSON.stringify(tokenData));
//                                             this.opentokenModel.build(tokenData).save().then(function (resultData) {
//                                                 // var mapObj={};
//                                                 // mapObj.rate_limit_per_min=0;
//                                                 // Globals.rateLimitMap.set(hub,mapObj);
//                                                 var obj = Object.assign({}, resultData.dataValues)
//                                                 sendingRequestToSubHub(obj, "allsubscribe");

//                                                 delete obj._id;
//                                                 delete obj.token_create_date;
//                                                 delete obj.user_name

//                                                 console.log("saved token details are 2 ", obj);
//                                                 return res.status(200).send({ status: 1, message: "Successfully retrieved.", data: obj });
//                                             }).catch(function (err) {
//                                                 console.log("error " + JSON.stringify(err.message));
//                                                 return res.status(500).send({ status: 0, message: "Something went wrong. Please try again later." });
//                                             })
//                                         }
//                                     })
//                                 }
//                                 else {
//                                     return res.status(200).send({ status: 0, message: "Invalid client_id." });
//                                 }
//                             })
//                         }
//                         else {
//                             return res.status(401).send({ status: 0, message: "Invalid code." });

//                         }

//                     }
//                 })
//             }
//             else if (grant_type == "refresh_token")// then is when access token expires then client ask for new access token
//             {
//                 if (!req.body.refresh_token) {
//                     return res.status(400).send({ status: 0, message: "Required parameters are missing." });
//                 }
//                 this.openapiModel.findAll({ where: { client_id: client_id } }).then(function (companyDetailsResult) {
//                     if (companyDetailsResult.length > 0) {
//                         var companyDetails = Object.assign({}, companyDetailsResult[0].dataValues);
//                         this.opentokenModel.findAll({ where: { hub_id: hub_id, company_id: companyDetails._id } }).then(function (hubTokenDetails) {
//                             if (hubTokenDetails.length > 0) {
//                                 if (hubTokenDetails[0].refreshtoken == req.body.refresh_token) {
//                                     var access_token = generateSessionId();
//                                     var refresh_token = hubTokenDetails[0].user_id + "_" + generateSessionId();

//                                     var expiryDate = new Date();
//                                     var tokenData = {};
//                                     expiryDate.setMonth(expiryDate.getMonth() + 3);
//                                     tokenData.accesstoken = access_token;
//                                     tokenData.refreshtoken = refresh_token;
//                                     tokenData.expiry_date = expiryDate;

//                                     console.log("mysql saving data" + JSON.stringify(tokenData));

//                                     hubTokenDetails[0].update(tokenData).then(function (resultData) {
//                                         // var mapObj={};
//                                         // mapObj.rate_limit_per_min=0;
//                                         // Globals.rateLimitMap.set(hub,mapObj);
//                                         console.log("saved token details are refresh token " + JSON.stringify(resultData));
//                                         var obj = Object.assign({}, resultData.dataValues)
//                                         delete obj._id;
//                                         delete obj.token_create_date;
//                                         delete obj.user_name

//                                         return res.status(200).send({ status: 1, message: "Successfully retrieved.", data: obj });
//                                     }).catch(function (err) {
//                                         console.log("error " + JSON.stringify(err.message));
//                                         return res.status(500).send({ status: 0, message: "Something went wrong. Please try again later." });
//                                     })
//                                 }
//                                 else {
//                                     return res.status(401).send({ status: 0, message: "Invalid refresh_token." });
//                                 }

//                             }
//                             else {
//                                 return res.status(401).send({ status: 0, message: "Invalid hub_id." });
//                             }
//                         })


//                     }
//                     else {
//                         return res.status(401).send({ status: 0, message: "Invalid client_id." });
//                     }
//                 })
//             }
//             else {
//                 return res.status(401).send({ status: 0, message: "Invalid grand_type." });
//             }
//         }
//         else {
//             return res.status(400).send({ status: 0, message: "Session has expired. Please try again." });
//         }
//     }
// });

// router.post('/socket/v2/unsubscribeAll', function (req, res, next) {

//     if (!req.headers.access_token ||
//         !req.headers.ts ||
//         !req.body.hub_id) {
//         return res.status(400).send({ status: 0, message: "Required parameters are missing." });
//     }

//     var timestamp = req.headers.ts;
//     var hub_id = req.body.hub_id;

//     if (((new Date().getTime()) - timestamp * 1000 <= 30000))//30 seconds
//     {
//         validateSession(req, res, function (err, sessionDetails) {
//             if (err) {
//                 return res.status(500).send({ status: 0, message: "Something went wrong. Please try again later." });
//             }
//             else {
//                 getModels();
//                 console.log("unsubscribe all ",sessionDetails.company_id);
//                 this.opentokenModel.destroy({ where: { company_id: sessionDetails.company_id, hub_id: hub_id } }).then(function () {
//                     this.socketModel.findAll({ where: { hub_id: hub_id } }).then(function (socketDetails) {
//                         _.each(socketDetails, function (eachSocketDetails) {
//                             if (eachSocketDetails) {
//                                console.log("Un subscribe all device is "+JSON.stringify(eachSocketDetails) );
//                                  sendingRequestToSubHub(eachSocketDetails, "unsubscribe")
//                             }
//                         })
//                         return res.status(200).send({ status: 1, message: "Logged out successfully." });
//                     }).catch(function () {
//                         console.log("error in getting socket details");
//                         return res.status(500).send({ status: 0, message: "Something went wrong. Please try again later." });
//                     })
//                 }).catch(function (error) {
//                     console.log("error in destroying company details",JSON.stringify(error.message));
//                     return res.status(500).send({ status: 0, message: "Something went wrong. Please try again later." });
//                 })

//             }
//         })
//     }
//     else {
//         return res.status(400).send({ status: 0, message: "Session has expired. Please try again." });
//     }


// })
function encryptPassword(id) {
    var sha = crypto.createHash('md5');
    sha.update(id);
    return sha.digest('hex');
};
function generateSessionId() {
    var sha = crypto.createHash('sha1');
    sha.update(Math.random().toString());
    return sha.digest('hex');
};
function sendingRequestToSubHub(hubCompanyDetails, url) {
    var options = {
        method: 'POST',
        url: "http://smartsockets.b1automation.com:8080/smartsocket/" + url,
        'headers': {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(hubCompanyDetails)
    };
    request(options, function (error, response, rsbody) {
        if (error) {
            console.log("error in sub for a hub." + JSON.stringify(error));
        } else {
            console.log("Successfully unsubscribed.");
        }
    })
}
function validateSession(req, res, cb) {
    var token = req.headers.access_token.split(" ")[1];
    var hub_id = req.body.hub_id;
    console.log("validate session ", token);
    console.log("validate session ", hub_id);

    getModels();
    this.opentokenModel.findAll({ where: { hub_id: hub_id } }).then(function (sessionDetails) {
        if (sessionDetails.length > 0) {
            //console.log("sessionDetails ", sessionDetails[0].accesstoken);
            if (sessionDetails[0].accesstoken == token) {
                cb(null, sessionDetails[0])
            }
            else {
                return res.status(401).send({ status: 0, message: "Session has expired." });
            }
        }
        else {
            return res.status(500).send({ status: 0, message: "Invalid details. Please try again." });
        }
    }).catch(function (err) {
        return res.status(500).send({ status: 0, message: "Something went wrong. Please try again later." });
    })

}
 
// An encrypt function
function encrypt(text,key,iv) {

    // Creating Cipheriv with its parameter
    let cipher = crypto.createCipheriv(algorithm, Buffer.from(key), iv);

    // Updating text
    let encrypted = cipher.update(text);

    // Using concatenation
    encrypted = Buffer.concat([encrypted, cipher.final()]);
    return {
        key: key.toString('hex'), 
        type: iv.toString('hex'),
        data: encrypted.toString('hex')
    };
}

function decrypt(text, key1, iv1) {
    let key = Buffer.from(key1, 'hex');
    let iv = Buffer.from(iv1, 'hex');
    let encryptedText = Buffer.from(text, 'hex');
    console.log(key.length);
    console.log(iv.length);

    // Creating Decipher
    let decipher = crypto.createDecipheriv(algorithm, key, iv);

    // Updating encrypted text
    let decrypted = decipher.update(encryptedText);
    decrypted = Buffer.concat([decrypted, decipher.final()]);

    // returns data after decryption
    return decrypted.toString();
} 

// function encrypt(text,ENCRYPTION_KEY) {

//     let iv = crypto.randomBytes(IV_LENGTH);
//     let cipher = crypto.createCipheriv(algorithm, Buffer.from(ENCRYPTION_KEY, 'hex'), iv);
//     let encrypted = cipher.update(text);
//     encrypted = Buffer.concat([encrypted, cipher.final()]);
//     return iv.toString('hex') + ':' + encrypted.toString('hex');
// }

// function decrypt(text,ENCRYPTION_KEY) {

//     let textParts = text.split(':');
//     let iv = Buffer.from(textParts.shift(), 'hex');
//     let encryptedText = Buffer.from(textParts.join(':'), 'hex');
//     let decipher = crypto.createDecipheriv(algorithm, Buffer.from(ENCRYPTION_KEY, 'hex'), iv);
//     let decrypted = decipher.update(encryptedText);
//     decrypted = Buffer.concat([decrypted, decipher.final()]);
//     return decrypted.toString();

// }
// function encrypt(text, password) {
//     var cipher = crypto.createCipher(algorithm, password)
//     var crypted = cipher.update(text, 'utf8', 'hex')
//     crypted += cipher.final('hex');
//     return crypted;
// }
// function decrypt(text, password) {
//     var decipher = crypto.createDecipher(algorithm, password)
//     var dec = decipher.update(text, 'hex', 'utf8')
//     dec += decipher.final('utf8');
//     return dec;
// }

function getModels() {

    var Models = db.getModels();
    this.userModel = Models.User;
    this.userHubModel = Models.UserHub;
    this.userDevModel = Models.UserDev;
    this.hubModel = Models.Hub;
    this.hubDefModel = Models.HubDef;
    this.roomModel = Models.Room;
    this.hubRoomModel = Models.HubRoom;
    this.hubEmergConts = Models.HubEmergConts;
    this.deviceModel = Models.Devices;
    this.remoteModel = Models.RemoteKeyInfo;
    this.userPrefModel = Models.UserPreference;
    this.openapiModel = Models.OpenApi;
    this.opentokenModel = Models.OpenTokens;
    this.deviceCategoryModel = Models.DeviceCategory;
    this.socketModel = Models.Sockets;
    this.smartThingsModel = Models.SmartThings;
};
module.exports = router;
