var redis = require('redis'),
    db = require('./mySqlDB'),
    client;
    
var Models = db.getModels();
exports.connect = function (host, port, cb) {
    client = redis.createClient(port, host);

    client.on('connect', function () {
        cb(null,"");
    });

    client.on('error', function (err) {
        console.log('Unable to connect to Redis.' + err);
        cb(err,null)
    });
};

exports.setData = function (key, obj, cb) {
   // console.log("inside redis methids is ",obj)
var bool=false;
    if(obj.minu5Data ||obj.powerValue || obj.dayData )
        {
            if(obj.isFromSocketEnerygyValues && obj.isFromSocketEnerygyValues=="1"){
                delete obj.isFromSocketEnerygyValues
                bool=true;
            }

        }
       // console.log("inside redis methids is ",obj)
       // console.log("inside redis methids is ",bool)

    client.hmset(key, obj, function (err, object) {
        if(bool==true){
            cb(err, obj);
        }
        else{
            cb(err, object);
        }
        //console.log(object);
    });
};

exports.setDataTest = function (key, obj, cb) {
    cb(err, object);
};
exports.getEmailData = function (key, cb) {
    console.log("key is :" + key);
    client.get(key, function (err, object) {
        console.log("object  is :" + object);
        if (cb) {
            cb(err, JSON.parse(object));
        }

    });
};

exports.getStringData = function(key, cb){
    //console.log("key is :" + key);
    client.get(key, function(err, object) {
        console.log("redis key stringdata  is :" +key +"::"+ object);
        if(cb){ cb(err, object); }

    });
};

exports.setDataWithTTL = function (key, obj, ttlInSec) {
    console.log("key data to set :" + obj);
    console.log("key value to set :" + key);
    client.set(key, obj, 'EX', ttlInSec);
};
exports.getData = function (key, cb) {


    //console.log("key is :" + key);
    client.hgetall(key, function (err, object) {

        if (cb) {
            cb(err, object);
        }

    });
};


exports.getDataPromise = function (key) {
    return new Promise(function (resolve, reject) {
        //console.log("key is :" + key);
        client.hgetall(key, function (err, hubObj) {
            if (hubObj) {
                if (hubObj == null || hubObj == undefined) {
                    //console.log("hubObjct in error from promise "+JSON.stringify(hubObj));
                    resolve(hubObj);
                } else {
                    var hubtime = {};
                    hubtime.hub_status_date = hubObj.hub_status_date;
                    hubtime.hub_status = "1";
                    Models.Hub.update(hubtime, {
                            where: {
                                hub_id: hubObj.hub_id
                            }
                        })
                        .then(function (recs) {
                            ///console.log(" from promise record db updated");
                            resolve(recs);
                        })
                        .catch(function (err) {
                            console.log("error in promise  db updating" + err.message);
                            resolve(err);
                        });
                }
            } else {
                return "fail";
            }
        });
    });
};


exports.getPromiseforOffline = function (key) {

    return new Promise(function (resolve, reject) {
        //console.log("key is :" + key);
        client.hgetall(key, function (err, hubObj) {

            if (hubObj == null || hubObj == undefined) {
                //console.log("hubObjct in error from promise "+JSON.stringify(hubObj));
                resolve(null);
            } else {

                resolve(hubObj);

            }


        });
    });
}


exports.getPromiseforObjData = function (key) {

    return new Promise(function (resolve, reject) {
        //console.log("key is :" + key);
        client.hgetall(key, function (err, obj) {

            if (obj == null || obj == undefined) {
                //console.log("hubObjct in error from promise "+JSON.stringify(hubObj));
                resolve(null);
            } else {

                resolve(obj);

            }



        });
    });


};

exports.getAllKeys = function (keyPattern, cb) {
    client.keys(keyPattern, function (err, keys) {
        if (err) return console.log(err);
        cb(err, keys);

    });
};


exports.isKeyExists = function (key, cb) {
    client.exists(key, function (err, reply) {
        if (reply === 1) {
            cb(true);

        } else {
            console.log('doesn\'t exist');
            cb(false);
        }
    });
};
exports.isKeyExistsPromise = function (key) {

    return new Promise(function (resolve, reject) {
        client.exists(key, function (err, reply) {
            if (reply == 1) {
                resolve("exists");

            } else {
                //console.log('doesn\'t exist');
                resolve(null);
            }
        });
    });
};

exports.delData = function (key, cb) {
    client.del(key, function (err, reply) {
        console.log("reply");
        cb(err, reply);
    });
};
exports.rename = function (oldkey,newKey, cb) {
    client.rename(oldkey,newKey,function(err, reply){
        console.log("reply");
        cb(err, reply);
    })
  
};