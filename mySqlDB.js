var Sequelize = require('sequelize');
var Users = require('./Models/users');
var UserHub = require('./Models/userHub');
var UserDev = require('./Models/userDevice');
var DeviceCategory = require('./Models/deviceCategories');
var Devices = require('./Models/devices');
var Hub = require('./Models/hub');
var HubDef = require('./Models/hubDefinition');
var HubRoom = require('./Models/hubRoom');
var Room = require('./Models/room');
var HubEmergencyContacts = require('./Models/hubEmergencyContacts');
var RemoteKeys = require('./Models/remoteKeys');
var AlexaUser = require('./Models/alexaUser');
var AlexaUserCustom = require('./Models/alexaUserCustom');
var GhomeUser = require('./Models/ghomeUser');
var LineUser = require('./Models/lineUser');
var MessageList = require('./Models/message');
var FbUser = require('./Models/fb_user');
var LSAHubs = require('./Models/lsaHubs.js');
var UserPreference = require('./Models/userPreference');
var AuthParam = require('./Models/authParam');
var GetHubFirmware = require('./Models/getHubFirmwareDetails');
var LsaMiniHub = require('./Models/lsaminiHubs');
var ghomeCustomUser = require("./Models/ghomeCustomUser");
var SmartThingUser = require('./Models/smartThingsUser');
var IFTTTUser = require('./Models/IFTTTUser');
var CellularDetails = require('./Models/cellularDetails');

var InternalOAuthParam = require('./Models/IntAuthParam');
var ThirdPartyAccountInfo = require('./Models/thirdPartyAccountInfo');
var SmartThings=require('./Models/smartthing');
var ExtenderDevices=require('./Models/extender_devices');
var Locations=require('./Models/hubLocations');
var EnergyMeter=require('./Models/energyMeter');
//AI
var AiStaticData=require('./Models/aiCategoryList');
var AiConfig=require('./Models/aiConfig');
var AiHomeInfo=require('./Models/aiHomeIfnormation');
var AiUserLifeStyle=require('./Models/aiUserLifestyle');
var Extender=require('./Models/extender');
var NestUser=require('./Models/nestUser');
var Dth=require('./Models/dth');
var RemoteCount=require('./Models/remotecount');
var OpenApi=require('./Models/openApiComapny');
var OpenTokens=require('./Models/openApiTokendata');
var HubAutoUpdate=require('./Models/hubAutoUpdation');
// Production
var EdgeProd=require('./Models/boneedgeprod');
var SocketProd=require('./Models/bonesocketprod');
var NilmDeviceDetails= require('./Models/nilm_device_details');

var Sockets=require('./Models/sockets');
var UserBudget = require('./Models/userbudget');
var Currency = require('./Models/currency');

var mySqlConnection = null;
var models = {};


exports.connect = function (host, port, dbName, userName, password, redisHost, redisPort, cb) {
    var sequelize = new Sequelize(dbName, userName, password, {
        // default db values
        // host: localhost,
        // port: 3306,
        host: host,
        port: port,
        dialect: 'mysql',
        logging: false,
        syncOnAssociation: true,
        pool: {
            max: 50,
            min: 10,
            idle: 60000
        },
        define: {
            charset: 'utf8',
            collate: 'utf8_unicode_ci',
            timestamps: false
        }
    });
    
    models.User = sequelize.define('User', Users.getSchema(), {
        tableName: Users.getTableName()
    });
    models.UserHub = sequelize.define('UserHub', UserHub.getSchema(), {
        tableName: UserHub.getTableName()
    });
    models.UserDev = sequelize.define('UserDev', UserDev.getSchema(), {
        tableName: UserDev.getTableName()
    });
    models.DeviceCategory = sequelize.define('DeviceCategory', DeviceCategory.getSchema(), {
        tableName: DeviceCategory.getTableName()
    });
    models.Devices = sequelize.define('Devices', Devices.getSchema(), {
        tableName: Devices.getTableName()
    });
    models.Hub = sequelize.define('Hub', Hub.getSchema(), {
        tableName: Hub.getTableName()
    });
    models.HubDef = sequelize.define('HubDef', HubDef.getSchema(), {
        tableName: HubDef.getTableName()
    });
    models.HubRoom = sequelize.define('HubRoom', HubRoom.getSchema(), {
        tableName: HubRoom.getTableName()
    });
    models.Room = sequelize.define('Room', Room.getSchema(), {
        tableName: Room.getTableName()
    });
    models.HubEmergConts = sequelize.define('HubEmergConts', HubEmergencyContacts.getSchema(), {
        tableName: HubEmergencyContacts.getTableName()
    });
    models.Currency = sequelize.define('Currency', Currency.getSchema(), {
        tableName: Currency.getTableName()
    });
    
    models.RemoteKeyInfo = sequelize.define('RemoteKeyInfo', RemoteKeys.getSchema(), {
        tableName: RemoteKeys.getTableName()
    });
    models.AlexaUser = sequelize.define('AlexaUser', AlexaUser.getSchema(), {
        tableName: AlexaUser.getTableName()
    });
    models.AlexaUserCustom = sequelize.define('AlexaUserCustom', AlexaUserCustom.getSchema(), {
        tableName: AlexaUserCustom.getTableName()
    });
    models.LineUser = sequelize.define('LineUser', LineUser.getSchema(), {
        tableName: LineUser.getTableName()
    });
    models.MessageList = sequelize.define('MessageList', MessageList.getSchema(), {
        tableName: MessageList.getTableName()
    });
    models.FbUser = sequelize.define('FbUser', FbUser.getSchema(), {
        tableName: FbUser.getTableName()
    });
    models.GhomeUser = sequelize.define('GhomeUser', GhomeUser.getSchema(), {
        tableName: GhomeUser.getTableName()
    });
    models.UserPreference = sequelize.define('UserPreference', UserPreference.getSchema(), {
        tableName: UserPreference.getTableName()
    });
    models.LSAHubs = sequelize.define('LSAHubs', LSAHubs.getSchema(), {
        tableName: LSAHubs.getTableName()
    });
     models.AuthParam = sequelize.define('AuthParam', AuthParam.getSchema(), {tableName: AuthParam.getTableName()});
    models.GetHubFirmware = sequelize.define('gethubFirmware', GetHubFirmware.getSchema(), {
        tableName: GetHubFirmware.getTableName()
    });
    models.LsaMiniHub = sequelize.define("LsaMiniHub", LsaMiniHub.getSchema(), {
        tableName: LsaMiniHub.getTableName()
    });
    models.ghomeCustomUser = sequelize.define('ghomeCustomUser', ghomeCustomUser.getSchema(), {
        tableName: ghomeCustomUser.getTableName()
    });
    models.ThirdPartyAccountInfo = sequelize.define('ThirdPartyAccountInfo', ThirdPartyAccountInfo.getSchema(), {
        tableName: ThirdPartyAccountInfo.getTableName()
    });
    models.SmartThingUser = sequelize.define('SmartThingUser', SmartThingUser.getSchema(), {
        tableName: SmartThingUser.getTableName()
    });
    models.IFTTTUser = sequelize.define('IFTTTUser', IFTTTUser.getSchema(), {
        tableName: IFTTTUser.getTableName()
    });

    models.CellularDetails = sequelize.define('CellularDetails', CellularDetails.getSchema(), {
        tableName: CellularDetails.getTableName()
    });

    models.InternalOAuthParam = sequelize.define('InternalOAuthParam', InternalOAuthParam.getSchema(), {
        tableName: InternalOAuthParam.getTableName()
    });
    models.Locations = sequelize.define('Locations', Locations.getSchema(), {
        tableName: Locations.getTableName()
    });

    models.EnergyMeter = sequelize.define('EnergyMeter', EnergyMeter.getSchema(), {
        tableName: EnergyMeter.getTableName()
    });
    
    
    models.Extender=sequelize.define('Extender', Extender.getSchema(), {tableName: Extender.getTableName()});
    models.SmartThings=sequelize.define('SmartThings', SmartThings.getSchema(), {tableName: SmartThings.getTableName()});
    models.AiStaticData=sequelize.define('AiStaticData', AiStaticData.getSchema(), {tableName: AiStaticData.getTableName()});
    models.AiConfig=sequelize.define('AiConfig', AiConfig.getSchema(), {tableName: AiConfig.getTableName()});
    models.AiHomeInfo=sequelize.define('AiHomeInfo', AiHomeInfo.getSchema(), {tableName: AiHomeInfo.getTableName()});
    models.AiUserLifeStyle=sequelize.define('AiUserLifeStyle', AiUserLifeStyle.getSchema(), {tableName: AiUserLifeStyle.getTableName()});
    models.NestUser=sequelize.define('NestUser', NestUser.getSchema(), {tableName: NestUser.getTableName()});
    models.Dth=sequelize.define('Dth', Dth.getSchema(), {tableName: Dth.getTableName()});
    models.RemoteCount=sequelize.define('RemoteCount', RemoteCount.getSchema(), {tableName: RemoteCount.getTableName()});
    models.OpenApi=sequelize.define('OpenApi', OpenApi.getSchema(), {tableName: OpenApi.getTableName()});
    models.OpenTokens=sequelize.define('OpenTokens', OpenTokens.getSchema(), {tableName: OpenTokens.getTableName()});
    models.HubAutoUpdate=sequelize.define('HubAutoUpdate', HubAutoUpdate.getSchema(), {tableName: HubAutoUpdate.getTableName()});
    // sequelize.sync({force:true}).then(function(){    // Note: This will delete the tables and create them again.
    models.EdgeProd=sequelize.define('EdgeProd', EdgeProd.getSchema(), {tableName: EdgeProd.getTableName()});
    models.SocketProd=sequelize.define('SocketProd', SocketProd.getSchema(), {tableName: SocketProd.getTableName()});
    models.NilmDeviceDetails=sequelize.define("NilmDeviceDetails",NilmDeviceDetails.getSchema(),{tableName:NilmDeviceDetails.getTableName()});
    models.ExtenderDevices=sequelize.define('ExtenderDevices', ExtenderDevices.getSchema(), {tableName: ExtenderDevices.getTableName()});
    models.Sockets=sequelize.define("Sockets",Sockets.getSchema(),{tableName:Sockets.getTableName()});
    models.UserBudget=sequelize.define("UserBudget",UserBudget.getSchema(),{tableName:UserBudget.getTableName()});
    
    sequelize.sync({
        alter: false
    }).then(function () {
        mySqlConnection = sequelize;
        cb();
    }).catch(function (err) {
        console.log("Error while connecting to Db: " + JSON.stringify(err));
    });
    // console.log("Initializing sequel cache to redis is started");
    // sequelize.initCacheStore(redisHost, redisPort, {
    //     cacheStore: 'redis',
    //     cachePrefix: 'sql_cache'
    // });
    // console.log("Initializing sequel cache to redis is done ");

    // models.Devices.makeCache({
    //     ttl: 1200
    // });
    // models.HubDef.makeCache({
    //     ttl: 1200
    // });

    // models.Room.makeCache({
    //     ttl: 1200
    // });





};

exports.getModels = function () {
    return models;
};

exports.getUserModel = function () {
    return models.User;
};

exports.getUserHubModel = function () {
    return models.UserHub;
};

exports.getUserDeviceModel = function () {
    return models.UserDev;
};
exports.getMySqlConnection = function () {
    return mySqlConnection;
};