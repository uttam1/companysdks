var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
//var logger = require('morgan');
var http = require('http');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var app = express();
const dotenv = require('dotenv');
var mysqlDB=require('./mySqlDB');
var redis = require('./redisDB');
var mqtt = require('mqtt');

dotenv.config();
var port =  '3000';

var MysqlconnectionDetails = {
  host: process.env.PROD_MYSQL_HOST,
  username: process.env.PROD_MYSQL_HOST,
  password: process.env.PROD_MYSQL_HOST,
  database: process.env.PROD_MYSQL_HOST
}
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('port', port);

//app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With,Host, Content-Type, Accept, Authorization");
  next();
});
app.use('/', indexRouter);
app.use('/sdk/users', usersRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});


// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

process.argv.forEach(function (val, index, array) {
  console.log(index + ': ' + val);
  if (index == 2 && val != '' && val != 'undefined' && val != null) {
    // console.log('comming to condition ');
    if (val == 'stage' || val.indexOf('stage') > -1) {
      MysqlconnectionDetails = {
        host: process.env.STAGE_MYSQL_HOST,
        username: process.env.STAGE_MYSQL_USER,
        password: process.env.STAGE_MYSQL_PASSWORD,
        database: process.env.STAGE_MYSQL_DATABASE,
        mysqlport: process.env.STAGE_MYSQL_PORT,
        redisHost: process.env.STAGE_REDIS_HOST,
        redisPort: process.env.STAGE_REDIS_PORT,
        baseurl:process.env.STAGE_BASE_URL,
        mqttUrl:process.env.STAGE_MQTT_URL,
        mqttUsername:process.env.STAGE_MQTT_USER_NAME,
        mqttPassword:process.env.STAGE_MQTT_PASSWORD
      }
    }
    else {
      MysqlconnectionDetails = {
        host: process.env.PROD_MYSQL_HOST,
        username: process.env.PROD_MYSQL_USER,
        password: process.env.PROD_MYSQL_PASSWORD,
        database: process.env.PROD_MYSQL_DATABASE,
        mysqlport: process.env.PROD_MYSQL_PORT,
        redisHost: process.env.PROD_REDIS_HOST,
        redisPort: process.env.PROD_REDIS_PORT,
        baseurl:process.env.PROD_BASE_URL,
        mqttUrl:process.env.PROD_MQTT_URL,
        mqttUsername:process.env.PROD_MQTT_USER_NAME,
        mqttPassword:process.env.PROD_MQTT_PASSWORD,
      }
    }
  }
})
console.log("env details are ",MysqlconnectionDetails);
var server = http.createServer(app);

var opts = {
  keepalive: 10,
  clientId:'mqtt-app-server-'+Math.random().toString(16).substr(2, 8),
  protocolId: 'MQTT',
  protocolVersion: 4,
  clean: true,
  reconnectPeriod: 5000,
  username: MysqlconnectionDetails.mqttUsername,
  password: MysqlconnectionDetails.mqttPassword,
  queueQoSZero: false,
  connectTimeout : 4000,
  qos: 2,
  resubscribe: true
};


mysqlDB.connect(MysqlconnectionDetails.host, MysqlconnectionDetails.mysqlport, MysqlconnectionDetails.database, MysqlconnectionDetails.username, MysqlconnectionDetails.password,MysqlconnectionDetails.redisHost,MysqlconnectionDetails.redisPort, function (err) {
  if (err) {
    console.log('Unable to connect to MySql.'+JSON.stringify(err.message) );
    process.exit(1);
  } else {
    console.log('connected to MySql.');
    redis.connect(MysqlconnectionDetails.redisHost, MysqlconnectionDetails.redisPort, function(err,result){
      if(err)
      {
        console.log('Unable to connect to redis.'+JSON.stringify(err.message) );
        process.exit(1);
      }
      else{
        console.log("redis connected.");
        
        server.listen(port);
        server.on('error', onError);
        server.on('listening', onListening);
        global.baseurl=MysqlconnectionDetails.baseurl;
        console.log('API\'s work at '+global.baseurl+":"+ port + " url.");

        global.mqttConnection = mqtt.connect(MysqlconnectionDetails.mqttUrl, opts);

        global.mqttConnection.on('connect', function (connack) {
          console.log('mqqtt connection established ..' +JSON.stringify(connack));
        })

      }
   });
  }
});
/**
 * Get port from environment and store in Express.
 */





function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
}

module.exports = app;
