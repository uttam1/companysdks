var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    _id: { type: Sequelize.CHAR(10), primaryKey: true, defaultValue: Sequelize.UUIDV4 },
    region_name: { type: Sequelize.STRING },
    region_id: { type: Sequelize.STRING },
    provider_name: { type: Sequelize.STRING },
    provider_id: { type: Sequelize.STRING },
    channel_type: { type: Sequelize.STRING },
    channel_name: { type: Sequelize.STRING },
    channel_number: { type: Sequelize.STRING },
    channel_icon: { type: Sequelize.STRING },
   
   
  };
};

exports.getTableName = function(){
  return "b1_dth_data";
};