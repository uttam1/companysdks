var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    id: { type: Sequelize.CHAR(36), primaryKey: true, defaultValue: Sequelize.UUIDV4 },
    nest_access_token: { type: Sequelize.STRING },
    hub_id: { type: Sequelize.STRING },
    create_date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    modified_date:{ type: Sequelize.DATE, defaultValue: Sequelize.NOW },
  };
};

exports.getTableName = function(){
  return "b1_nest_data";
};
