var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    id: { type: Sequelize.CHAR(36), primaryKey: true, defaultValue: Sequelize.UUIDV4 },
    extender_id: { type: Sequelize.STRING },
    hub_id: { type: Sequelize.STRING },
    user_id: { type: Sequelize.CHAR(36) },
    device_name:{type: Sequelize.STRING},
    device_desc: { type: Sequelize.STRING},
    model_no: { type: Sequelize.STRING }, 
    date_of_mfg: { type: Sequelize.STRING },
    software_version: { type: Sequelize.STRING },
    software_install_date: { type: Sequelize.STRING },
    is_enabled:  { type: Sequelize.BOOLEAN, defaultValue: true }
   
  };
};

exports.getTableName = function(){
  return "b1_extenders";
};
