var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    id: { type: Sequelize.INTEGER,primaryKey: true,autoIncrement: true }, 
    hub_id: { type: Sequelize.STRING },
    remote_count: { type: Sequelize.STRING },
    create_date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW }
  };
};

exports.getTableName = function(){
  return "b1_remote_count";
};
