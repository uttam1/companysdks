var Sequelize = require('sequelize');
exports.getSchema = function() {
  return {
    id: { type: Sequelize.INTEGER,primaryKey: true,autoIncrement: true  },
    hub_id: { type: Sequelize.STRING, defaultValue: null },
    mac_id:{type: Sequelize.STRING, defaultValue: null},
    amount:{type: Sequelize.STRING, defaultValue: null},
    cost_per_unit:{type: Sequelize.STRING, defaultValue: null},
    month_year:{type:Sequelize.STRING,defaultValue:null},
    create_date:{type:Sequelize.DATE,defaultValue: Sequelize.NOW },
    modify_date:{type:Sequelize.DATE,defaultValue: Sequelize.NOW }
  };
};

exports.getTableName = function(){
  return "b1_user_budget";
};
