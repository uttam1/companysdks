var Sequelize = require('sequelize');
exports.getSchema = function() {
  return {
    socket_id: { type: Sequelize.INTEGER,primaryKey: true,autoIncrement: true  },
    user_id:{ type: Sequelize.CHAR(36)},
    hub_id: { type: Sequelize.STRING, defaultValue: null },
    device_name:{ type: Sequelize.STRING, defaultValue: null },
    mac_id:{type: Sequelize.STRING, defaultValue: null},
    socket_model:{type: Sequelize.STRING, defaultValue: "0" },
    device_id: { type: Sequelize.CHAR(36)},
    device_b_one_id: { type: Sequelize.CHAR(40) },
    category_id: { type: Sequelize.CHAR(36) }
  };
};

exports.getTableName = function(){
  return "b1_sockets";
};
