var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    auth_id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
    client_id: { type: Sequelize.STRING, defaultValue: null },
    client_secret: { type: Sequelize.STRING, defaultValue: null },
    basic_auth_token: { type: Sequelize.STRING(1000), defaultValue: null },
    ip_addr_csv: { type: Sequelize.STRING(2000), defaultValue: null },
    rate_limit_per_min: { type: Sequelize.INTEGER, defaultValue: 10000},
    is_active: { type: Sequelize.BOOLEAN, defaultValue: true },
    creation_date : {type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    client_display_name: { type: Sequelize.STRING, defaultValue: null },
    redirect_url_auth : { type: Sequelize.STRING(1000), defaultValue: null },
    callback_url_device_info : { type: Sequelize.STRING(1000), defaultValue: null },
    client_url_login_report_info : { type: Sequelize.STRING(1000), defaultValue: null },
      origin_id: { type: Sequelize.INTEGER }
  };
};

exports.getTableName = function(){
  return "b1_internal_auth_param";;
};
