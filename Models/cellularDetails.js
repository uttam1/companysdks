var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    id: { type: Sequelize.INTEGER,primaryKey: true,autoIncrement: true },
    cellular_name: { type: Sequelize.STRING },
    cellular_apn: { type: Sequelize.STRING },
    country_code: { type: Sequelize.STRING }
  };
};

exports.getTableName = function(){
  return "b1_cellular_details";
};