var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    user_id: { type: Sequelize.CHAR(36), primaryKey: true, defaultValue: Sequelize.UUIDV4 },
    user_name: { type: Sequelize.STRING, defaultValue: null },
    first_name: { type: Sequelize.STRING, defaultValue: null },
    last_name: { type: Sequelize.STRING, defaultValue: null },
    address: { type: Sequelize.STRING(2048), defaultValue: null },
    email_id: { type: Sequelize.STRING, unique: true },
    phone_no: { type: Sequelize.STRING, defaultValue: "+91-9999999999"},
    alt_phone_no: { type: Sequelize.STRING, defaultValue: "+91-9999999999" },
    customer_pin: { type: Sequelize.CHAR(8), defaultValue: null },
    password: { type: Sequelize.STRING},
    old_password1: { type: Sequelize.STRING, defaultValue: null },
    old_password2: { type: Sequelize.STRING, defaultValue: null },
    security_question1: { type: Sequelize.STRING, defaultValue: null },
    security_answer1: { type: Sequelize.STRING, defaultValue: null },
    security_question2: { type: Sequelize.STRING, defaultValue: null },
    security_answer2: { type: Sequelize.STRING, defaultValue: null },
    act_create_date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    act_delete_date: { type: Sequelize.DATE, defaultValue: null },
    act_authenticated: { type: Sequelize.BOOLEAN, defaultValue: false },
    act_enabled: { type: Sequelize.BOOLEAN, defaultValue: true },
    authentication_code: { type: Sequelize.STRING, defaultValue: null },
    otp: { type: Sequelize.STRING, defaultValue: null },
    luid: { type: Sequelize.STRING, defaultValue: null },
    dob: { type: Sequelize.STRING, defaultValue: null },
    gender: { type: Sequelize.STRING, defaultValue: null },
    isMaster: { type: Sequelize.BOOLEAN, defaultValue: true },
    region:{ type: Sequelize.STRING(255), defaultValue: "US" },
    login_type:{type: Sequelize.ENUM('0', '1', '2'),defaultValue: "0"},
    uuid: { type: Sequelize.STRING, defaultValue: null }
//    ,
//    alexa_access_token: { type: Sequelize.STRING, defaultValue: null }
  };
};

exports.getTableName = function(){
  return "b1_user_details";
};
