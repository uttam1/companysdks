var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    auth_id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
    client_id: { type: Sequelize.STRING, defaultValue: null },
    client_secret: { type: Sequelize.STRING, defaultValue: null },
    basic_auth_token: { type: Sequelize.STRING(1000), defaultValue: null },
    ip_addr_csv: { type: Sequelize.STRING(2000), defaultValue: null },
    rate_limit_per_min: { type: Sequelize.INTEGER, defaultValue: 10000},
    is_active: { type: Sequelize.BOOLEAN, defaultValue: true },
    creation_date : {type: Sequelize.DATE, defaultValue: Sequelize.NOW }
  };
};

exports.getTableName = function(){
  return "b1_auth_param";
};
