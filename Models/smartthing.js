var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    st_id : { type: Sequelize.CHAR(36), primaryKey: true, defaultValue: Sequelize.UUIDV4 },
    install_app_id: { type: Sequelize.STRING, defaultValue: null },
    location_id: { type: Sequelize.STRING, defaultValue: null },
    user_id : { type: Sequelize.STRING, defaultValue: null },
    email_id : { type: Sequelize.STRING, defaultValue: null },
    hub_id : { type: Sequelize.STRING(2000), defaultValue: null },
    access_token : { type: Sequelize.STRING(2000), defaultValue: null },
    refresh_token : { type: Sequelize.STRING(2000), defaultValue: null },
    access_token_expiry :{type: Sequelize.DATE },
    state: { type: Sequelize.STRING, defaultValue: null },
    session_id: { type: Sequelize.STRING, defaultValue: null},
    count: { type: Sequelize.INTEGER, defaultValue: 1 }
  };
};

exports.getTableName = function(){
  return "b1_smartthing";
};
