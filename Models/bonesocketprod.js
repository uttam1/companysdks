var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    socket_id: { type: Sequelize.INTEGER,primaryKey: true,autoIncrement: true  },
    settings: { type: Sequelize.STRING, defaultValue: null },
    created_time:{ type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    start_time: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    user_id:{ type: Sequelize.CHAR(36)},
    hub_id: { type: Sequelize.STRING, defaultValue: null },
    tester_name:{ type: Sequelize.STRING, defaultValue: null },
    ssid_list: { type: Sequelize.STRING(2048), defaultValue: null },
    logo_red: { type: Sequelize.STRING, defaultValue: "0" },
    turn_off_by_slide_switch: { type: Sequelize.STRING, defaultValue: "0" },
    calibration_expected_value_status: { type: Sequelize.STRING, defaultValue: "0" },
    current:{type: Sequelize.STRING, defaultValue: "0"},
    input_current:{type: Sequelize.STRING, defaultValue: "0"},
    input_resistance:{type: Sequelize.STRING, defaultValue: "0"},
    input_voltage:{type: Sequelize.STRING, defaultValue: "0"},
    input_watt:{type: Sequelize.STRING, defaultValue: "0"},
    is_calibration_success:{type: Sequelize.STRING, defaultValue: "0"},
    is_connected_mqtt:{type: Sequelize.STRING, defaultValue: "0"},
    is_connecting_tcp:{type: Sequelize.STRING, defaultValue: "0"},
    off_status_controlling_from_app:{type: Sequelize.STRING, defaultValue: "0"},
    on_status_controlling_from_app:{type: Sequelize.STRING, defaultValue: "0"},
    power:{type: Sequelize.STRING, defaultValue: "0"},
    voltage:{type: Sequelize.STRING, defaultValue: "0"},
    device_model:{type: Sequelize.STRING, defaultValue: null},
    mac_id:{type: Sequelize.STRING, defaultValue: null},
    end_time: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    socket_led_color:{type: Sequelize.STRING, defaultValue: "0"},
    led_blue:{type: Sequelize.STRING, defaultValue: "0"},
    total_time: { type: Sequelize.STRING, defaultValue: "" },
    final_result: { type: Sequelize.STRING, defaultValue: "0" },
    socket_model:{type: Sequelize.STRING, defaultValue: "0" },
    moreinfo:{ type: Sequelize.STRING(1024), defaultValue: null },
    before_calibration_current:{type: Sequelize.STRING, defaultValue: null},
    before_calibration_power:{type: Sequelize.STRING, defaultValue: null},
    before_calibration_voltage:{type: Sequelize.STRING, defaultValue: null},
    connect_load_to_the_socket:{type: Sequelize.STRING, defaultValue: null},
    off_status_controlling_from_device:{type: Sequelize.STRING, defaultValue: null},
    on_status_controlling_from_device:{type: Sequelize.STRING, defaultValue: null},
    socket_connected_to_power:{type: Sequelize.STRING, defaultValue: null}
    


  };
};

exports.getTableName = function(){
  return "b1_socket_prod";
};
