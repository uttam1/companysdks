var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    _id: { type: Sequelize.CHAR(36), primaryKey: true, defaultValue: Sequelize.UUIDV4 },
    model_name: { type: Sequelize.STRING,defaultValue:null },
    model_type: { type: Sequelize.ENUM('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15'), defaultValue: '1' },
    client_id: { type: Sequelize.STRING ,defaultValue:null},
    client_secret_key: { type: Sequelize.STRING,defaultValue:null },
    redirection_URL: { type: Sequelize.STRING ,defaultValue:null},
    refresh_token: { type: Sequelize.STRING,defaultValue:null },
    expiry_date: { type: Sequelize.DATE, defaultValue:null },
    create_date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    modified_date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    modified_by:{ type: Sequelize.CHAR(36),defaultValue:null }
  };
};

exports.getTableName = function(){
  return "b1_thirdparty_account_info";
};