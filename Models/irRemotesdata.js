var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    id: { type: Sequelize.INTEGER,primaryKey: true,autoIncrement: true },
    type: { type: Sequelize.STRING, defaultValue: "Ceiling Light" },
    brand: { type: Sequelize.STRING},
    model: { type: Sequelize.STRING },
    key_number: { type: Sequelize.STRING, defaultValue: null},
    key_name: { type: Sequelize.STRING, defaultValue: null },
    key_irdata: { type: Sequelize.STRING, defaultValue: null },
    create_date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    is_active: { type: Sequelize.BOOLEAN, defaultValue: true }

  };
};

exports.getTableName = function(){
  return "b1_ir_remotes_data";
};
