var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    id: { type: Sequelize.INTEGER,primaryKey: true,autoIncrement: true },
    user_id: { type: Sequelize.STRING },
    hub_id: { type: Sequelize.STRING },
    home_type: { type: Sequelize.STRING ,defaultValue: "1" },
    owner: { type: Sequelize.STRING,defaultValue: "1"  },
    home_size: { type: Sequelize.STRING,defaultValue: "1"  },
    is_active:{ type: Sequelize.BOOLEAN, defaultValue: true },
    create_date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    modified_date:{ type: Sequelize.DATE, defaultValue: Sequelize.NOW }
  };
};

exports.getTableName = function(){
  return "b1_ai_home_information";
};
