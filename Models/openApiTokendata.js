var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    _id: { type: Sequelize.CHAR(36), primaryKey: true, defaultValue: Sequelize.UUIDV4 },
    user_id: { type: Sequelize.STRING },
    company_id:{type: Sequelize.STRING},
    user_name: { type: Sequelize.STRING, defaultValue: null },
    email_id: { type: Sequelize.STRING, defaultValue: null },
    hub_id: { type: Sequelize.STRING, defaultValue: null },
    accesstoken: { type: Sequelize.STRING, defaultValue: null },
    refreshtoken: { type: Sequelize.STRING, defaultValue: null },
    token_create_date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    expiry_date: { type: Sequelize.DATE }
  };
};

exports.getTableName = function(){
  return "b1_openapi_tokens";
};