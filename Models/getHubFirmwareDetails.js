var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    id: { type: Sequelize.INTEGER,primaryKey: true,autoIncrement: true  },
    ble: { type: Sequelize.STRING, defaultValue: null },
    zigbee: { type: Sequelize.STRING, defaultValue: null },
    hub: { type: Sequelize.STRING, defaultValue: null },
    mini_hub:{type: Sequelize.STRING, defaultValue: '1.0,Rev01'},
    remote_server:{type: Sequelize.STRING, defaultValue: ''},
    remote_base_url:{type: Sequelize.STRING, defaultValue: ''},
    app_ios: { type: Sequelize.STRING, defaultValue: null},
    app_android: { type: Sequelize.STRING, defaultValue: null},
    app_f: { type: Sequelize.STRING, defaultValue: null},
    created_date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    ble_f: { type: Sequelize.STRING, defaultValue: ''},
    zigbee_f: { type: Sequelize.STRING, defaultValue: ''},
    hub_f: { type: Sequelize.STRING, defaultValue: ''},
    mini_hub_f: { type: Sequelize.STRING, defaultValue: ''}
  };
};

exports.getTableName = function(){
  return "b1_hub_app_firmware_details";
};
