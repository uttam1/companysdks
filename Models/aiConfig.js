var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    id: { type: Sequelize.INTEGER,primaryKey: true,autoIncrement: true },
    user_id: { type: Sequelize.STRING },
    hub_id: { type: Sequelize.STRING },
    ai_status: { type: Sequelize.STRING,defaultValue:"1" },
    activity: { type: Sequelize.STRING,defaultValue:"00" },
    clothing: { type: Sequelize.STRING,defaultValue:"00" },
    ai_intervel: { type: Sequelize.STRING,defaultValue:"1" },
    enable_date: { type: Sequelize.STRING,defaultValue: "1,2,3,4,5,6,7"},
    enable_time_start: { type: Sequelize.STRING,defaultValue:"00:00"  },
    enable_time_end: { type: Sequelize.STRING,defaultValue: "23:59" },
    device_id: { type: Sequelize.STRING },
    device_b_one_id: { type: Sequelize.STRING },
    user_pref_temp: { type: Sequelize.STRING,defaultValue:"1" },
    is_active:{ type: Sequelize.BOOLEAN, defaultValue: true },
    create_date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    modified_date:{ type: Sequelize.DATE, defaultValue: Sequelize.NOW }
    // enable_time_start_hour: { type: Sequelize.INTEGER },
    // enable_time_start_min: { type: Sequelize.INTEGER },
    // enable_time_end_hour: { type: Sequelize.INTEGER },
    // enable_time_end_min: { type: Sequelize.INTEGER }


  };
};

exports.getTableName = function(){
  return "b1_ai_config";
};
