var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    id: { type: Sequelize.INTEGER,primaryKey: true,autoIncrement: true },
    user_id:{ type: Sequelize.STRING, defaultValue: null },
    hub_id: { type: Sequelize.STRING, defaultValue: null },
    device_name: { type: Sequelize.STRING, defaultValue: null },
    mac_id:{type: Sequelize.STRING, defaultValue: null},
    category:{type: Sequelize.STRING, defaultValue: null},
    status: { type: Sequelize.STRING, defaultValue: "0"},
    status_date: { type: Sequelize.BIGINT},
    count:{ type: Sequelize.INTEGER, defaultValue: 0},
    power: { type: Sequelize.STRING, defaultValue: "0"},
    phase: { type: Sequelize.STRING, defaultValue: "1"},
    unique_id: { type: Sequelize.STRING, defaultValue: null},
    created_date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    modified_data: { type: Sequelize.DATE, defaultValue: Sequelize.NOW }  ,
    device_id: { type: Sequelize.STRING },
    device_b_one_id: { type: Sequelize.CHAR(40) },
    device_state: { type: Sequelize.ENUM('ADDED', 'DELETED'), defaultValue: 'ADDED' },
  };
};

exports.getTableName = function(){
  return "b1_nilm_device_detail_status";
};