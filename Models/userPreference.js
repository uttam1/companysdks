var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
  	  id: { type: Sequelize.CHAR(36), primaryKey: true, defaultValue: Sequelize.UUIDV4 },
      user_id: { type: Sequelize.CHAR(36) },
      hub_id: { type: Sequelize.STRING },
      isMaster: { type: Sequelize.BOOLEAN, defaultValue: true },
      device_id_list: {type: Sequelize.STRING(5000) },
      room_id_list: { type: Sequelize.STRING(5000) },
      rule_id_list: { type: Sequelize.STRING(5000) },
      mode:{ type: Sequelize.STRING,defaultValue: "0" },
      device:{ type: Sequelize.STRING,defaultValue: "0" },
      room:{ type: Sequelize.STRING,defaultValue: "0" },
      rule:{ type: Sequelize.STRING,defaultValue: "0"},
      line_notify : { type: Sequelize.STRING,defaultValue: "1"},
      fb_notify : { type: Sequelize.STRING,defaultValue: "1" },
      app_notify : { type: Sequelize.STRING,defaultValue: "1" },
      favorites: { type: Sequelize.STRING(4000),defaultValue: "" },
      is_active : {type: Sequelize.BOOLEAN, defaultValue: true}
  };
};

exports.getTableName = function(){
  return "b1_user_preference";
};
