var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    id: { type: Sequelize.INTEGER,primaryKey: true,autoIncrement: true },
    user_id: { type: Sequelize.STRING },
    physical_activity: { type: Sequelize.STRING ,defaultValue: "1" },
    wake_time: { type: Sequelize.STRING ,defaultValue: "1" },
    sleep_time: { type: Sequelize.STRING ,defaultValue: "1" },
    body_type: { type: Sequelize.STRING ,defaultValue: "1" },
    is_active:{ type: Sequelize.BOOLEAN, defaultValue: true },
    create_date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    modified_date:{ type: Sequelize.DATE, defaultValue: Sequelize.NOW }
  };
};

exports.getTableName = function(){
  return "b1_ai_user_lifestyle";
};
