var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    key_id: { type: Sequelize.CHAR(36), primaryKey: true, defaultValue: Sequelize.UUIDV4 },
    device_id: { type: Sequelize.CHAR(36), allowNull: false },
    key_type: { type: Sequelize.ENUM("Fixed", "Custom", "Sequence", "Optional") },
    key_option: { type: Sequelize.CHAR(10) },
    key_state: { type: Sequelize.ENUM("UNPAIRED", "PAIRED") },
    key_number: { type: Sequelize.INTEGER },
    key_data: { type: Sequelize.STRING(2048) },
    key_desc: { type: Sequelize.CHAR(20) }
  };
};

exports.getTableName = function(){
  return "b1_remote_keys_info";
};