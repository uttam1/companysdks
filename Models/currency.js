var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    s_no: { type: Sequelize.INTEGER,primaryKey: true,autoIncrement: true  },
    country:{type: Sequelize.STRING,defaultValue:null},
    currency:{type: Sequelize.STRING,defaultValue:null},
    code:{type: Sequelize.STRING,defaultValue:null},
    symbol:{type: Sequelize.TEXT,defaultValue:null},
    cost_per_unit:{type: Sequelize.STRING,defaultValue:"0"}
  };
};

exports.getTableName = function(){
  return "b1_countries_currency";
};
