var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    lsa_id: { type: Sequelize.INTEGER,primaryKey: true,autoIncrement: true  },
    settings: { type: Sequelize.STRING, defaultValue: null },
    created_time:{ type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    start_time: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    user_id:{ type: Sequelize.CHAR(36)},
    hub_id: { type: Sequelize.STRING, defaultValue: null },
    tester_name:{ type: Sequelize.STRING, defaultValue: null },
    ssid_list: { type: Sequelize.STRING(2048), defaultValue: null },
    hub_logo_color: { type: Sequelize.STRING, defaultValue: "0" },
    ir_added: { type: Sequelize.STRING, defaultValue: "0" },
    hub_restart: { type: Sequelize.STRING, defaultValue: "0" },
    power_push: { type: Sequelize.STRING, defaultValue: "0" },
    ir_working: { type: Sequelize.STRING, defaultValue: "0" },
    ir_learning_working: { type: Sequelize.STRING, defaultValue: "0" },
    delete_ir: { type: Sequelize.STRING, defaultValue: "0" },
    reset_hub: { type: Sequelize.STRING, defaultValue: "0" },
    logo_red: { type: Sequelize.STRING, defaultValue: "0" },
    end_time: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    total_time: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    final_result: { type: Sequelize.STRING, defaultValue: "0" },
    moreinfo:{ type: Sequelize.STRING(1024), defaultValue: null },
    red_led_blinkon_irkeyfire: { type: Sequelize.STRING, defaultValue: null },
    sevenir_ledblaston_irkeyfire: { type: Sequelize.STRING, defaultValue: null },
    lux_value: { type: Sequelize.STRING, defaultValue: null },
    temperature_value: { type: Sequelize.STRING, defaultValue: null }
  };
};

exports.getTableName = function(){
  return "b1_lsa_minis";
};