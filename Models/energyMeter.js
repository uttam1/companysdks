var Sequelize = require('sequelize');
exports.getSchema = function() {
  return {
    socket_id: { type: Sequelize.INTEGER,primaryKey: true,autoIncrement: true  },
    user_id:{ type: Sequelize.CHAR(36)},
    hub_id: { type: Sequelize.STRING, defaultValue: null },
    device_name:{ type: Sequelize.STRING, defaultValue: null },
    mac_id:{type: Sequelize.STRING, defaultValue: null},
    socket_model:{type: Sequelize.STRING, defaultValue: "0" },
    device_id: { type: Sequelize.CHAR(36)},
    device_b_one_id: { type: Sequelize.CHAR(40) },
    category_id: { type: Sequelize.CHAR(36) },
    latitude:{type: Sequelize.STRING, defaultValue: "0" },
    longitude:{type: Sequelize.STRING, defaultValue: "0" },
    is_online:{type: Sequelize.STRING, defaultValue: "1" },
    P1LVL:{type: Sequelize.STRING, defaultValue: "0" },
    P2LVL:{type: Sequelize.STRING, defaultValue: "0" },
    P3LVL:{type: Sequelize.STRING, defaultValue: "0" },
    P1UVL:{type: Sequelize.STRING, defaultValue: "0" },
    P2UVL:{type: Sequelize.STRING, defaultValue: "0" },
    P3UVL:{type: Sequelize.STRING, defaultValue: "0" }
  };
};

exports.getTableName = function(){
  return "b1_energy_meters";
};
