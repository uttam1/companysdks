var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    device_id: { type: Sequelize.CHAR(36), primaryKey: true },
    device_b_one_id: { type: Sequelize.CHAR(40) },
    extender_id :  { type: Sequelize.STRING },
    hub_id :  { type: Sequelize.STRING },
    category_id: { type: Sequelize.CHAR(36) },
    device_name: { type: Sequelize.STRING },
    device_state: { type: Sequelize.ENUM('ADDED', 'DELETED'), defaultValue: 'ADDED' },
    device_create_date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    room_id: { type: Sequelize.CHAR(36) }
  };
};

exports.getTableName = function(){
  return "b1_extender_devices";
};