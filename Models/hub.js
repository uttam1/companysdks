var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    id: { type: Sequelize.CHAR(36), primaryKey: true, defaultValue: Sequelize.UUIDV4 },
    hub_id: { type: Sequelize.STRING },
    model_no: { type: Sequelize.STRING },
    password: { type: Sequelize.STRING},
    date_of_mfg: { type: Sequelize.STRING },
    software_version: { type: Sequelize.STRING },
    software_install_date: { type: Sequelize.STRING },
    hub_install_date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    hub_status:{ type: Sequelize.STRING,defaultValue: "1"},
    hub_status_date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    hub_app_version:{ type: Sequelize.STRING, defaultValue: "0" },
    is_mqtt_enabled:  { type: Sequelize.BOOLEAN, defaultValue: false },
    is_New_Hub:{ type: Sequelize.STRING, defaultValue: "1" }
  };
};

exports.getTableName = function(){
  return "b1_hubs";
};
