var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    device_id: { type: Sequelize.CHAR(36), primaryKey: true, defaultValue: Sequelize.UUIDV4 },
    device_b_one_id: { type: Sequelize.CHAR(40) },
    category_id: { type: Sequelize.CHAR(36) },
    device_name: { type: Sequelize.STRING },
    device_state: { type: Sequelize.ENUM('ADDED', 'DELETED'), defaultValue: 'ADDED' },
    device_create_date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    device_delete_date: { type: Sequelize.DATE },
    device_deleted_by: { type: Sequelize.CHAR(36) },
    room_id: { type: Sequelize.CHAR(36) },
    notify_me: { type: Sequelize.BOOLEAN, defaultValue: false },
    security_mode: { type: Sequelize.ENUM('Disarm', 'Arm', 'Inhouse', 'Panic', 'Medical Emergency'), defaultValue: 'Disarm' },
    key_count: { type: Sequelize.INTEGER, defaultValue: 0 },
    protocol: { type: Sequelize.ENUM('Infrared', 'wi-fi', 'ethrenet', 'RS232', '') }
  };
};

exports.getTableName = function(){
  return "b1_devices";
};