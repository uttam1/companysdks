var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
  //  record_id: { type: Sequelize.INTEGER,  autoIncrement: true,    primaryKey: true  },
    message_id: { type: Sequelize.INTEGER,primaryKey: true},
    interface_category: { type: Sequelize.STRING },
    message_category: { type: Sequelize.STRING },
    lang_eng: { type: Sequelize.STRING(2000)   },
    lang_jpn: { type: Sequelize.STRING(2000)   },
    comments: { type: Sequelize.STRING(2000)   },
    lang_1: { type: Sequelize.STRING(2000)   },
    lang_2: { type: Sequelize.STRING(2000)   },
    lang_3: { type: Sequelize.STRING(2000)   },
    lang_4: { type: Sequelize.STRING(2000)   },
    lang_5: { type: Sequelize.STRING(2000)   },
    added_by: { type: Sequelize.STRING },
    created_date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    modified_date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },



  };
};

exports.getTableName = function(){
  return "b1_message_list";
};
