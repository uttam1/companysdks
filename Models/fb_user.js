var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    fb_id: { type: Sequelize.CHAR(36),primaryKey: true  },
    user_id: { type: Sequelize.STRING, defaultValue: null },
    email_id: { type: Sequelize.STRING, defaultValue: null },
    hub_id: { type: Sequelize.STRING, defaultValue: null },
    customer_pin: { type: Sequelize.STRING, defaultValue: null},
    session_id: { type: Sequelize.STRING, defaultValue: null},
    state: { type: Sequelize.STRING, defaultValue: null },
    count: { type: Sequelize.INTEGER, defaultValue: 1 }
  };
};

exports.getTableName = function(){
  return "b1_fb_user";
};
