var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    pk_id : { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
    user_name: { type: Sequelize.STRING, defaultValue: null },
    user_id : { type: Sequelize.STRING, defaultValue: null },
    email_id : { type: Sequelize.STRING, defaultValue: null },
    hub_id : { type: Sequelize.STRING(2000), defaultValue: null },
    access_token : { type: Sequelize.STRING(2000), defaultValue: null },
    refresh_token : { type: Sequelize.STRING(2000), defaultValue: null },
    access_token_expiry :{type: Sequelize.DATE },
    token_scope : {type: Sequelize.STRING, defaultValue: null },
    is_active: { type: Sequelize.BOOLEAN, defaultValue: true },
    receiving_client_id :{ type: Sequelize.STRING, defaultValue: null },
    receiving_state :{ type: Sequelize.STRING, defaultValue: null },
    is_revoked :  { type: Sequelize.BOOLEAN, defaultValue: false },
    revoking_client_id :{ type: Sequelize.STRING, defaultValue: null },
    revoked_date :  { type: Sequelize.BOOLEAN, defaultValue: false },
    creation_date : {type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    last_modified_date :  {type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    user_display_name: { type: Sequelize.STRING, defaultValue: null },
    description: { type: Sequelize.STRING, defaultValue: null }
  };
};

exports.getTableName = function(){
  return "b1_oauth_smartthings_user_token";
};
