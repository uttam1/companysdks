var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    id: { type: Sequelize.CHAR(36),primaryKey: true, defaultValue: Sequelize.UUIDV4 },
    hub_id: { type: Sequelize.STRING },
    device_name: { type: Sequelize.STRING },
    device_desc: { type: Sequelize.STRING },
    hub_longitude: { type: Sequelize.STRING },
    hub_latitude: { type: Sequelize.STRING },
    hub_address: {type: Sequelize.STRING(5000),defaultValue:""  },
    location_name: {type: Sequelize.STRING,defaultValue:"" },
    zip_code: {type: Sequelize.STRING,defaultValue:"" },
    state: {type: Sequelize.STRING,defaultValue:"" },
    city: {type: Sequelize.STRING(500),defaultValue:"" },
    address: {type: Sequelize.STRING(5000),defaultValue:"" }
    

    // no_of_rooms: { type: Sequelize.INTEGER, defaultValue: 0 }
  };
};

exports.getTableName = function(){
  return "b1_hub_definition";
};