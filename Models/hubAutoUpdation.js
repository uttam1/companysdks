var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    id: { type: Sequelize.CHAR(36),primaryKey: true, defaultValue: Sequelize.UUIDV4 },
    hub_id: { type: Sequelize.STRING },
    to_user: { type: Sequelize.STRING },
    from_user: { type: Sequelize.STRING },
    hub_changed_date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW }
  };
};

exports.getTableName = function(){
  return "b1_hub_autoupdation";
};