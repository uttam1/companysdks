var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    id: { type: Sequelize.INTEGER,primaryKey: true,autoIncrement: true },
    category: { type: Sequelize.STRING },
    sub_category: { type: Sequelize.STRING },
    key: { type: Sequelize.STRING },
    en: { type: Sequelize.STRING },
    jp: { type: Sequelize.STRING },
    is_active:{ type: Sequelize.BOOLEAN, defaultValue: true },
    create_date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    modified_date:{ type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    sort_order:{type: Sequelize.STRING}
  };
};

exports.getTableName = function(){
  return "b1_ai_static_data";
};
