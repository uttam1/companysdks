
var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    location_id: { type: Sequelize.CHAR(36), primaryKey: true, defaultValue: Sequelize.UUIDV4 },
    hub_id: { type: Sequelize.STRING },
    user_id:{ type: Sequelize.STRING },
    location_name: {type: Sequelize.STRING,defaultValue:"" },
    location_type: {type: Sequelize.STRING,defaultValue:"" },
    location_sqft: {type: Sequelize.STRING,defaultValue:"" },
    created_date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
  };
};

exports.getTableName = function(){
  return "b1_hub_locations";
};
