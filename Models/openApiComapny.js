var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    _id: { type: Sequelize.CHAR(36), primaryKey: true, defaultValue: Sequelize.UUIDV4 },
    company_name: { type: Sequelize.STRING,defaultValue:null },
    company_desc: { type: Sequelize.STRING ,defaultValue:null},
    redirection_url: { type: Sequelize.STRING ,defaultValue:null},
    location: { type: Sequelize.STRING,defaultValue:null },
    privacy_policy:{ type: Sequelize.STRING,defaultValue:null },
    email_id:{type: Sequelize.STRING ,defaultValue:null},
    contact_number:{type: Sequelize.STRING ,defaultValue:null},
    client_id: { type: Sequelize.CHAR(36), defaultValue: Sequelize.UUIDV4 },
    client_secret_key: { type: Sequelize.CHAR(36),defaultValue: Sequelize.UUIDV4 },
    create_date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    active_status:{ type: Sequelize.BOOLEAN, defaultValue: false },
    purpose:{ type: Sequelize.STRING,defaultValue:null }
  };
};

exports.getTableName = function(){
  return "b1_openapi_company_info";
};